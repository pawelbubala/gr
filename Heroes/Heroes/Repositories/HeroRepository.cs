﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Heroes.Repositories
{
    public class HeroRepository
    {
        List<Models.Hero> heroes = new List<Models.Hero>();
        ContentManager contentManager;

        public HeroRepository(ContentManager contentManager)
        {
            this.contentManager = contentManager;
            heroes = populateHeroes();
        }

        public List<Models.Hero> populateHeroes()
        {
            heroes.Add(new Models.Hero("Aniol", 10, contentManager.Load<Texture2D>("Heroes/angel")));
            heroes.Add(new Models.Hero("Diabel", 12, contentManager.Load<Texture2D>("Heroes/devil")));

            return heroes;
        }

        public List<Models.Hero> findAll()
        {
            return heroes;
        }
    }
}

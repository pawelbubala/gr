﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Heroes.ObjectValue
{
    class Player : Interfaces.AnimatedInterface
    {
        public Services.AnimationService animatedPlayer;

        int movingSpeed = 4;
        public float angle = 0.0f;

        public Player(Services.AnimationService animatedPlayer)
        {
            this.animatedPlayer = animatedPlayer;
        }

        public void animate(Services.AnimationService animationService)
        {
        }

        public void move(int x, int y)
        {
            animatedPlayer.rectangle.X = x;
            animatedPlayer.rectangle.Y = y;
        }

        public void moveLeft()
        {
            animatedPlayer.rectangle.X -= movingSpeed;

            angle = -90.0f;
        }

        public void moveRight()
        {
            animatedPlayer.rectangle.X += movingSpeed;

            angle = 90.0f;
        }

        public void moveUp()
        {
            animatedPlayer.rectangle.Y -= movingSpeed;

            angle = 0.0f;
        }

        public void moveDown()
        {
            animatedPlayer.rectangle.Y += movingSpeed;

            angle = 180.0f;
        }
    }
}

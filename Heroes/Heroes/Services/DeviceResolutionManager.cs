﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes.Services
{
    public class DeviceResolutionManager
    {
        protected Viewport viewport;

        public DeviceResolutionManager(Viewport viewport)
        {
            this.viewport = viewport;
        }

        public int computeWidth(int percentageWidth)
        {
            return this.viewport.Width / 100 * percentageWidth;

        }

        public int computeHeight(int percentageHeight)
        {
            return this.viewport.Height / 100 * percentageHeight;
        }
    }
}

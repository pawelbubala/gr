﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Heroes.Services
{
    class AnimationService
    {
        public  delegate void animatedMethod(AnimationService animation);
        public animatedMethod animationMethodDelegate;
        public Texture2D texture;
        public Rectangle rectangle, playerAreaRectangle;
        public float elapsedTime;
        public float fps = 0.30f;
        public bool isFinished = false;
        int x = 0, y = 0;

        public AnimationService(Texture2D texture, Rectangle rectangle, Rectangle playerAreaRectangle, float fps)
        {
            this.texture = texture;
            this.rectangle = rectangle;
            this.playerAreaRectangle = playerAreaRectangle;
            this.fps = fps;
            this.elapsedTime = fps;
        }

        public void addToDelegate(animatedMethod Method)
        {
            animationMethodDelegate += Method;
        }

        public void update()
        {
            this.x += 1;

            if (playerAreaRectangle.Y >= playerAreaRectangle.Height)
            {
                this.x = 0;
                this.y = 0;
            }
            else
            {
                if (playerAreaRectangle.X > playerAreaRectangle.Width)
                {
                    this.x = 0;
                    //this.y += 1;
                }
            }

            this.playerAreaRectangle.X = this.x * this.playerAreaRectangle.Width;
            this.playerAreaRectangle.Y = this.y * this.playerAreaRectangle.Height;
        }

        public void tick(GameTime gameTime)
        {
            elapsedTime = elapsedTime - gameTime.ElapsedGameTime.Milliseconds;

            if (elapsedTime < 0.0f)
            {
                elapsedTime = fps;
                animationMethodDelegate(this);
                update();
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Heroes
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Services.ViewEventListener viewEventListner;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.AllowUserResizing = false;
            IsMouseVisible = true;

            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
        }

        protected override void Initialize()
        {
            base.Initialize();

            viewEventListner = new Services.ViewEventListener(this);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            viewEventListner.dispatch(gameTime);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public SpriteBatch getSpriteBatch()
        {
            return spriteBatch;
        }

        public Services.ViewEventListener getViewEventListener()
        {
            return viewEventListner;
        }

        public Services.DeviceResolutionManager getDeviceResolutionManager()
        {
            return new Services.DeviceResolutionManager(this.GraphicsDevice.Viewport);
        }

        public Repositories.HeroRepository getHeroRepository()
        {
            return new Repositories.HeroRepository(Content);
        }
        
    }
}
